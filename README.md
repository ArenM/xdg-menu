# Desktop Entry Structure

```rust
struct DesktopEntry {
  comments: Option<Vec<comment>>,
  sections: HashMap<String, HashMap<String, String>>
}

example: DesktopEntry {
  comments: ["Comment"],
  sections: HashMap {
    "DeskopEntry": HashMap {
      "$comm1": "This is a comment inside a section",
      "Key": "Value"
    },
    "Other Section": HashMap {
      "Key": "Value"
    }
  }
}
```
