#![feature(drain_filter, try_trait, try_blocks)]

pub mod parser;
mod types;

pub use types::*;

use std::path::PathBuf;

pub fn list_application_files() -> Vec<PathBuf> {
    let dirs = xdg::BaseDirectories::new().expect("error");
    dirs.list_data_files_once("applications")
}

pub fn list_application_objects() -> (Vec<DesktopEntry>, Vec<ParseResult>) {
    let mut raw: Vec<Result<DesktopEntry, ParseResult>> = list_application_files()
        .iter()
        .map(|p| DesktopEntry::from_path(p))
        .collect();

    let errors: Vec<ParseResult> = raw
        .drain_filter(|err| err.is_err())
        .filter_map(|e| {
        e.err()})
        .collect();

    let entries: Vec<DesktopEntry> = raw
        .drain_filter(|err| err.is_ok())
        .filter_map(|e| {
            let e = e.ok();
            e
        })
        .collect();

    (entries, errors)
}
