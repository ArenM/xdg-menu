use nom::{
    branch::alt,
    bytes::complete::{tag, take_till1, take_while1},
    combinator::opt,
    multi::many1,
    sequence::terminated,
    IResult,
};

use super::{is_alphanumeric, utils};
use crate::types::{KVLine, KValue};

use std::collections::HashMap;

struct Kvp {
    table: HashMap<String, KValue>,
}

impl Kvp {
    pub fn new() -> Self {
        let t: HashMap<String, KValue> = [(String::from("Name"), KValue::String(String::from("")))]
            .iter()
            .cloned()
            .collect();

        Self { table: t }
    }

    pub fn get(&self, key: &str) -> Option<&KValue> {
        self.table.get(key)
    }
}

pub fn parse(i: &str) -> IResult<&str, KVLine> {
    let extranumeric = "-[]";
    let (i, key) = take_while1(|c| is_alphanumeric(c as u8) || extranumeric.contains(c))(i)?;
    let (i, _) = utils::take_white(i)?;
    let (i, _) = tag("=")(i)?;
    let (i, _) = utils::take_white(i)?;
    let (i, v) = take_while1(|c| !utils::newline(c))(i)?;
    let (_, value) = parse_value(v, Some(key))?;

    Ok((
        i,
        KVLine {
            key: String::from(key),
            value,
        },
    ))
}

fn parse_value<'a>(i: &'a str, k: Option<&'a str>) -> IResult<&'a str, KValue> {
    let l = Kvp::new();
    match k {
        Some(k) => match l.get(k) {
            Some(t) => match t {
                // TODO: don't use general parsers
                KValue::List(_) => parse_list(i),
                KValue::Num(_) => alt((parse_list, parse_string))(i),
                KValue::String(_) => parse_string(i),
                KValue::Bool(_) => alt((parse_list, parse_string))(i),
            },
            None => alt((parse_list, parse_string))(i),
        },
        None => alt((parse_list, parse_string))(i),
    }
}

fn parse_list(i: &str) -> IResult<&str, KValue> {
    terminated(take_till1(|c| c == ';'), tag(";"))(i)?;

    let (i, v) = many1(|i| {
        let (i, r) = take_till1(|c| c == ';')(i)?;
        let (i, _) = opt(tag(";"))(i)?;
        Ok((i, r))
    })(i)?;

    let values: Vec<KValue> = v
        .iter()
        .filter_map(|s| parse_value(s, None).ok())
        .map(|s| s.1)
        .collect();

    Ok((i, KValue::List(values)))
}

fn parse_string(i: &str) -> IResult<&str, KValue> {
    let (i, v) = take_while1(|_| true)(i)?;
    Ok((i, KValue::String(String::from(v))))
}

#[cfg(test)]
mod tests {
    use super::*;
    use nom::error::ErrorKind;
    use nom::Err::Error;
    #[test]
    fn test_parser() {
        let res = parse("AZa-z09=Value");
        assert_eq!(
            res,
            Ok((
                "",
                KVLine {
                    key: String::from("AZa-z09"),
                    value: KValue::String(String::from("Value"))
                }
            ))
        );

        let res = parse("AZa-z09=va;vb");
        let res2 = parse("AZa-z09=va;vb;");
        assert_eq!(
            res,
            Ok((
                "",
                KVLine {
                    key: String::from("AZa-z09"),
                    value: KValue::List(vec!(
                        KValue::String(String::from("va")),
                        KValue::String(String::from("vb")),
                    ))
                }
            ))
        );
        assert_eq!(res, res2);

        let res = parse("K=");
        assert_eq!(res, Err(Error(("", ErrorKind::TakeWhile1))));

        let res = parse("=V");
        assert_eq!(res, Err(Error(("=V", ErrorKind::TakeWhile1))));
    }
}
