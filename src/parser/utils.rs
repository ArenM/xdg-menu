use nom::{
    bytes::complete::take_while,
    IResult,
};

pub fn newline(i: char) -> bool {
    "\n\r".contains(i)
}

pub fn take_whitespace(i: &str) -> IResult<&str, &str> {
    let chars = " \t\r\n";
    take_while(move |c| chars.contains(c))(i)
}

pub fn take_white(i: &str) -> IResult<&str, &str> {
    let chars = " \t";
    take_while(move |c| chars.contains(c))(i)
}

pub fn take_line(i: &str) -> IResult<&str, &str> {
    let (i, line) = take_while(|c| !newline(c))(i)?;
    let (i, _) = take_while(newline)(i)?;
    Ok((i, line))
}

