use std::process::Command;
use super::{DesktopEntry, Line, KValue};

use std::convert::TryFrom;

pub enum Error {
    NE,
    N
}

impl From<nom::Err<(&str, nom::error::ErrorKind)>> for Error {
    fn from(_: nom::Err<(&str, nom::error::ErrorKind)>) -> Self {
        Self::NE
    }
}

impl From<std::option::NoneError> for Error {
    fn from(_: std::option::NoneError) -> Self {
        Self::N
    }
}

#[derive(Debug, Clone)]
pub struct Exec {
    pub command: String,
    pub terminal: bool,
}

impl Into<Command> for Exec {
    fn into(self) -> Command {
        let mut com = match self.terminal {
            false => {
                let mut com = Command::new("sh");
                com.arg("-c");
                com
            },
            true => {
                let com = Command::new("xterm");
                com
            }
        };
        com.arg(self.command);
        com
    }
}

impl TryFrom<DesktopEntry> for Exec {
    type Error = Error;

    fn try_from(i: DesktopEntry) -> Result<Self, Self::Error> {
        let exec = match i.groups.get("Desktop Entry")?.get("Exec")? {
            Line::KeyVal(a) => match a.value.clone() {
                KValue::String(v) => Some(v),
                _ => None
            },
            _ => None,
        }?;

        let terminal: Result<bool, std::option::NoneError> = try {
            let term = match i.groups.get("Desktop Entry")?.get("Terminal")? {
                Line::KeyVal(a) => match a.value.clone() {
                    KValue::Bool(v) => Some(v),
                    _ => None
                },
                _ => None,
            }?;
            term
        };
        let terminal = terminal.unwrap_or(false);

        Ok(Self { command: String::from(exec), terminal})
    }
}
