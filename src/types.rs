pub mod line;

use indexmap::IndexMap;

use std::convert::From;
use std::fs::File;
use std::io::prelude::*;

use super::parser::parse_file;

#[derive(Debug, Clone)]
pub struct DesktopEntry {
    pub comments: Vec<String>,
    pub groups: IndexMap<String, IndexMap<String, Line>>,
}

#[derive(Debug)]
pub enum ParseResult {
    NomErr(nom::Err<(String, nom::error::ErrorKind)>),
    IOErr(std::io::Error),
}

impl From<std::io::Error> for ParseResult {
    fn from(error: std::io::Error) -> Self {
        Self::IOErr(error)
    }
}

impl From<nom::Err<(&str, nom::error::ErrorKind)>> for ParseResult {
    fn from(error: nom::Err<(&str, nom::error::ErrorKind)>) -> Self {
        match error {
            nom::Err::Incomplete(n) => Self::NomErr(nom::Err::Incomplete(n)),
            nom::Err::Error(e) => Self::NomErr(nom::Err::Error((String::from(e.0), e.1))),
            nom::Err::Failure(e) => Self::NomErr(nom::Err::Error((String::from(e.0), e.1))),
        }
    }
}

impl DesktopEntry {
    pub fn from_path(path: &std::path::Path) -> Result<Self, ParseResult> {
        let file = File::open(path)?;
        let mut buf_rdr = std::io::BufReader::new(file);
        let mut buffer = String::new();

        buf_rdr.read_to_string(&mut buffer)?;
        Self::from_string(&buffer)
    }
    pub fn from_string<'a>(file: &'a str) -> Result<Self, ParseResult> {
        let parse_result = parse_file(file)?;
        Ok(parse_result.1)
    }
}

#[derive(Debug, Clone)]
pub enum Line {
    Comment(String),
    Unknown(String),
    KeyVal(KVLine),
}

#[derive(Debug, Clone, PartialEq)]
pub struct KVLine {
    pub key: String,
    pub value: KValue,
}

#[derive(Debug, Clone, PartialEq)]
pub enum KValue {
    List(Vec<KValue>),
    Num(f64),
    String(String),
    Bool(bool),
}

pub trait KValueT: std::fmt::Debug + Clone + PartialEq {
    type Value;

    fn parse(i: &str) -> Self;
    fn result(&self) -> nom::IResult<&str, Self::Value>;
    fn get(&self) -> Self::Value;
}
