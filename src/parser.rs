mod keyval;
mod utils;

use nom::{
    branch::alt,
    bytes::complete::{tag, take_while, take_while1},
    character::is_alphanumeric,
    combinator::{map, not},
    multi::{many0, many1},
    sequence::delimited,
    IResult,
};

use crate::types::{DesktopEntry, KVLine, Line};
use indexmap::IndexMap;

fn parse_comment(i: &str) -> IResult<&str, &str> {
    let (i, _) = tag("#")(i)?;
    let (i, value) = utils::take_line(i)?;

    Ok((i, value))
}

fn parse_keyval(i: &str) -> IResult<&str, KVLine> {
    keyval::parse(i)
}

fn parse_unknown_line(i: &str) -> IResult<&str, &str> {
    let (i, line) = utils::take_line(i)?;
    let _ = take_while1(|_| true)(line)?;
    Ok((i, line))
}

fn parse_group_header(i: &str) -> IResult<&str, &str> {
    let (i, r) = delimited(
        tag("["),
        take_while(|c| !utils::newline(c) && c != ']'),
        tag("]"),
    )(i)?;
    let (i, _) = utils::take_whitespace(i)?;
    Ok((i, r))
}

fn parse_group(i: &str) -> IResult<&str, (&str, IndexMap<String, Line>)> {
    //let mut group: IndexMap<String, Line> = IndexMap::new();
    let mut comment_id = 0;

    let (i, group_name) = parse_group_header(i)?;

    let (i, mut lines) = many1(|i| {
        let (i, line) = utils::take_line(i)?;
        not(parse_group_header)(i)?;
        let (_, line) = alt((
            map(parse_keyval, |a| Line::KeyVal(a)),
            map(parse_comment, |s| Line::Comment(String::from(s))),
            map(parse_unknown_line, |l| Line::Unknown(String::from(l))),
        ))(line)?;
        Ok((i, line))
    })(i)?;

    let group: IndexMap<String, Line> = lines
        .drain(..)
        .filter_map(|l| match l.clone() {
            Line::KeyVal(v) => Some((String::from(v.key), l)),
            Line::Comment(_) => {
                let ret = (format!("$COM{}$", comment_id), l);
                comment_id += 1;
                Some(ret)
            }
            Line::Unknown(_) => {
                let ret = (format!("$UNK{}$", comment_id), l);
                comment_id += 1;
                Some(ret)
            }
        })
        .collect();

    Ok((i, (group_name, group)))
}

pub fn parse_file(i: &str) -> IResult<&str, DesktopEntry> {
    let mut groups: IndexMap<String, IndexMap<String, Line>> = IndexMap::new();
    // Grab comments from beginning of file
    let (i, comments) = many0(|i| {
        let (i, line) = utils::take_line(i)?;
        let (_, r) = parse_comment(line)?;
        Ok((i, String::from(r)))
    })(i)?;

    // Parse Groups
    let (i, mut groups_vec) = many1(parse_group)(i)?;
    groups_vec.reverse();

    for g in groups_vec.drain(..) {
        groups.insert(String::from(g.0), g.1);
    }

    Ok((
        i,
        DesktopEntry {
            comments: comments,
            groups,
        },
    ))
}

#[cfg(test)]
mod tests {
    use super::*;
    use super::utils::*;
    #[test]
    fn test_parser() {
        let c = "# This is a comment\n[This is a section]\n#and\nKey=value";
        let res = parse_file(c);
        println!("{:?}", res);
    }
    #[test]
    fn test_newline() {
        assert_eq!(newline(' '), false);
        assert_eq!(newline('\n'), true);
        assert_eq!(newline('\r'), true);
    }

    #[test]
    fn test_take_whitespace() {
        assert_eq!(take_whitespace(" a "), Ok(("a ", " ")));
        assert_eq!(
            take_whitespace("\n\n \r\n\t remainder "),
            Ok(("remainder ", "\n\n \r\n\t "))
        );
        assert_eq!(take_whitespace("\n"), Ok(("", "\n")));
        assert_eq!(take_whitespace("\r"), Ok(("", "\r")));
        assert_eq!(take_whitespace("\t"), Ok(("", "\t")));
        assert_eq!(take_whitespace(" "), Ok(("", " ")));
    }

    #[test]
    fn test_take_white() {
        assert_eq!(take_white(" a "), Ok(("a ", " ")));
        assert_eq!(take_white("\n"), Ok(("\n", "")));
        assert_eq!(take_white("\r"), Ok(("\r", "")));
        assert_eq!(take_white("\t"), Ok(("", "\t")));
        assert_eq!(take_white(" "), Ok(("", " ")));
    }

    #[test]
    fn test_take_line() {
        assert_eq!(take_line("line\nremainder"), Ok(("remainder", "line")));
        assert_eq!(take_line("line\r\nremainder"), Ok(("remainder", "line")));
        assert_eq!(
            take_line("line\r\n\tremainder"),
            Ok(("\tremainder", "line"))
        );
    }

    #[test]
    fn test_parse_comment() {
        assert_eq!(parse_comment("# comment"), Ok(("", " comment")));
        assert_eq!(parse_comment("# comment\n"), Ok(("", " comment")));
        assert_eq!(
            parse_comment("# comment \t\n\t newline"),
            Ok(("\t newline", " comment \t"))
        );
    }

    //#[test]
    /*fn test_parse_key_val() {
        let res = parse_keyval("AZa-z09=Value");
        assert_eq!(res, Ok(("", ("AZa-z09", "Value"))));

        let res = parse_keyval("K=");
        assert_eq!(res, Err(Error(("", ErrorKind::TakeWhile1))));

        let res = parse_keyval("=V");
        assert_eq!(res, Err(Error(("=V", ErrorKind::TakeWhile1))));
    }*/
}
